var suggestionbox = angular.module('suggestionbox', ['firebase']);

suggestionbox.controller('MainCtrl', function MainCtrl($scope, $firebaseArray, $firebaseObject) {
    // create a reference to our firebase database instance
    var firebaseRootUrl = 'https://suggestionboxfirebase.firebaseio.com/suggestions/';
    var fireRef = new Firebase(firebaseRootUrl);
    // save the database as an array and attach it to the controller's scope
    $scope.suggestions = $firebaseArray(fireRef);
    $scope.newSuggestion = '';
    // $scope.comments = $firebase(fireRef).child('comments').$asArray();
    $scope.newComment = '';

    //from the firebase youtube video
    //const rootRef = firebase.database().ref().child('suggestionboxfirebase');

    $scope.addSuggestion = function() {
        var newSuggestion = $scope.newSuggestion.trim();
        if (!newSuggestion.length) {
            return;
        }
        // push to firebase, using a method of the firebase array
        $scope.suggestions.$add({
            // var time = new Date().getTime();
            // var date = new Date(time);
            // var dateTime = date.toString();

            title: newSuggestion,
            date: Firebase.ServerValue.TIMESTAMP,
            upvotes: 0
                // userID and comments. how to add upvotes later? does this date work?
        });
        $scope.newSuggestion = '';
    };

    // delete from firebase, using a method of the firebase array
    $scope.removeSuggestion = function(suggestion) {
        $scope.suggestions.$remove(suggestion);
    };

    $scope.addComment = function(suggestion) {
        var newComment = suggestion.newComment.trim();
        // var newComment = "aesegg";
        if (!newComment.length) {
            return;
        }

        var dbSuggestion = new Firebase('https://suggestionboxfirebase.firebaseio.com/suggestions/' + suggestion.$id + '/comments');

        // push to firebase, using a method of the firebase array
        dbSuggestion.push({
            title: newComment,
            date: Firebase.ServerValue.TIMESTAMP,
            upvotes: 0
            // userID and comments
        });
        dbSuggestion.newComment = null;
    };

    // delete from firebase, using a method of the firebase array
    $scope.removeComment = function(comment) {
        $scope.comments.$remove(comment);
    };

    $scope.upVote = function(suggestion) {
        suggestion.upvotes += 1;
        $scope.suggestions.$save(suggestion);
    };

    $scope.downVote = function(suggestion) {
        // if (suggestion.upvotes <= 0) {
        //     suggestion.upvotes = 0;
        //     $scope.suggestions.$save(suggestion);
        // } else {
        //     suggestion.upvotes -= 1;
        //     $scope.suggestions.$save(suggestion);
        // }

        suggestion.upvotes -= 1;
        $scope.suggestions.$save(suggestion);
    };

    $scope.upVoteComment = function(suggestion, comment) {
        comment.upvotes += 1;
        var dbComment = new Firebase('https://suggestionboxfirebase.firebaseio.com/suggestions/' + suggestion.$id + '/comments/' + comment.$id);
        console.log(suggestion);
        dbComment.upvotes.$update(comment);
    };

    $scope.downVoteComment = function(comment) {
        // if (comment.upvotes <= 0) {
        //     comment.upvotes = 0;
        //     $scope.suggestions.$set(comment);
        // } else {
        //     comment.upvotes -= 1;
        //     $scope.suggestions.$set(comment);
        // }

        comment.upvotes -= 1;
        $scope.suggestions.$set(comment);
    };

    $scope.myOrder = '-date';

    $scope.orderByThis = function(x) {
        $scope.myOrder = x;
    }
});

// format input to stretch to fill div
// add back to top button?
// why is there an extra space only on the left of the first line of suggestion title before it wraps?
// can choose to show negative suggestions (add a button to show?)

// how to save to bitbucket, ignoring node modules and being able to look inside the public folder
// how to use firebase auth to have people not upvote more than once, can delete their own suggestions/comments
// upvotes not working- convert comments to angularfire
// make it responsive
